

var model = {
    boardSize: 7,
    numShips: 3,
    shipLength: 3,
    shipsSunk: 0,
    ships: [ 
            { locations: [0, 0, 0], hits: ["", "", ""]},
            { locations: [0, 0, 0], hits: ["", "", ""]},
            { locations: [0, 0, 0], hits: ["", "", ""]}           
           ],

    fire: function(guess) { // takes users guess as an argument = model.fire("50");
        for (var i = 0; i < this.numShips; i++) { // iterates ships 0-2
            var ship = this.ships[i];   // ship = ships arrays 0-2
            var index = ship.locations.indexOf(guess); // indexOf method searches an array for a matching value and returns its index
            if ( index >= 0) { // if user's guess matches index 0-2
                ship.hits[index] = "hit"; // INDEX IS VARIABLE and hits ARRAY, adds "hit" to hits arrays index 0-2
                view.displayHit(guess);   // calls view and gives the user's argument to view
                view.displayMessage("HIT!"); // calls view and tells View to display message
                if (this.isSunk(ship)) {
                    view.displayMessage("You sank my battleship!");
                    this.shipsSunk++; // adds +1 to shipsSunk
                }
                return true; // function returns true if a hit has occured
            }
        }
        view.displayMiss(guess); // executes displayMiss with guess value
        view.displayMessage("You missed!");
        return false; // if not, returns false when called

    },

    isSunk: function(ship) { 
        for (var i = 0; i < this.shipLength; i++) { //iterates through shipLength once
            if (ship.hits[i] !== "hit") { // ITERATES THIS 3 TIMES, checks if hits array index does not have "hit" on it
                return false; // returns false, so the ship is not sunk yet

            }
        }
        return true; // if all hits array 0-2 contains "hit", ship is sunk
    },

    generateShipLocations: function() {
        var locations;
        for (var i = 0; i < this.numShips; i++) { // for each ship
            do {
                locations = this.generateShip(); // uses method to generate ship for locations
            } while (this.collision(locations)); // checks for collisions with method
            this.ships[i].locations = locations; // adds ship to array
        }
        console.log("Ships array: ");
		console.log(this.ships);
    },

    generateShip: function() { // starts with creating a random direction
        var direction = Math.floor(Math.random() * 2); // generates a number 0-1
        var row;
        var col;
        if (direction === 1) { // 1 = horizontal ship, 0 vertical
            row = Math.floor(Math.random() * this.boardSize); // horizontal start position, result 0-6
            col = Math.floor(Math.random() * (this.boardSize - this.shipLength + 1));
        } else {
            row = Math.floor(Math.random() * (this.boardSize - this.shipLength + 1)); // vertical start position
            col = Math.floor(Math.random() * this.boardSize);
        }

        var newShipLocations = []; // creates new ship locations for empty array and adds them one by one
        for (var i = 0; i < this.shipLength; i++) { //looping number of locations
            if (direction === 1) { // horizontal ship locations, +2 columns
                newShipLocations.push(row + "" + (col + i)); // row location + col and 2 more locations by iterating into the array
            } else { // vertical ship locations
                newShipLocations.push((row + i) + "" + col); // col location + row and 2 more locations by iterating
            }
        } 
        return newShipLocations;
    },

    collision: function(locations) { // checks for collisions between existing ships and new ships locations
        for (var i = 0; i < this.numShips; i++) { // for each existing ship
            var ship = this.ships[i];
            for (var j = 0; j < locations.length; j++) { // check if any new ship location is the same as existing ship location in array
                if (ship.locations.indexOf(locations[j]) >= 0) { // if index is higher than 0, the location is in use, so we have a collision
                    return true; // returning inside a loop stops the iteration immediately
                }
            }
        } 
        return false; // no collision, no matches
    }
    

};

var view = {
    displayMessage: function(msg) {
        var messageArea = document.getElementById("messageArea");
        messageArea.innerHTML = msg;
    },

    displayHit: function(location) { // user's guess as argument
        var cell = document.getElementById(location); // takes the id of the user's guess td, like 50
        cell.setAttribute("class", "hit"); // adds class="hit" to that td so it runs .hit in css
    },

    displayMiss: function(location) {
        var cell = document.getElementById(location);
        cell.setAttribute("class", "miss");
    }
};

var controller = {
    guesses: 0,

    processGuess: function(guess) {
        var location = parseGuess(guess); // calls parseGuess and gives it's result in guess
        if (location) {
            this.guesses++;
            var hit = model.fire(location); // calls fire method and gives argument to it
            if (hit && model.shipsSunk === model.numShips) { // if shipsSunk and numShips are equal number
                view.displayMessage("You sank all my battleships, in " +
                this.guesses + " guesses."); // gives argument to view

            }
        }

    }
};



function parseGuess(guess) {
    var alphabet = ["A", "B", "C", "D", "E", "F", "G"]; // 0-6

    if (guess === null || guess.length !== 2) { // null = variable has no value
        alert("Please, enter a letter and a number on the board.");
    } else {
        var firstChar = guess.charAt(0);
        var row = alphabet.indexOf(firstChar); // returns -1 if not in array
        var column = guess.charAt(1);

        if (isNaN(row) || isNaN(column)) { // checks that both are numbers
            alert("Oops, that's not on the board!");
         } else if (row < 0 || row >= model.boardSize ||
                     column < 0 || column >= model.boardSize) { //check if number is 0-6
                        alert("Oops, that's off the board!");
            } else {
                return row + column;
            }
            
            
        }
        return null; // failed check along the way
    }





function handleFireButton() {
    var guessInput = document.getElementById("guessInput");
    var guess = guessInput.value.toUpperCase(); // user's guess is stored in the html's input's "value" property
    controller.processGuess(guess); // guess is a variable, contains user's input, passes it to processGuess function

    guessInput.value = ""; // resets the form text to empty string
}

function handleKeyPress(e) {
    var fireButton = document.getElementById("fireButton");
    if (e.keyCode === 13) { //keyCode: KeyboardEvent's (object) property 13, which is the Enter key
        fireButton.click(); // click is HTML's own method, "tricking" fireButton to think it was clicked
        return false; // false so the form doesn't do anything else like submit
    }

}

window.onload = init; // runs init function as soon as the page is fully loaded

function init() {
    var fireButton = document.getElementById("fireButton"); // gets fireButton id
    fireButton.onclick = handleFireButton; // adds a handleFireButton function to mouse click
    var guessInput = document.getElementById("guessInput");
    guessInput.onkeypress = handleKeyPress; // handleKeyPress: a new handler for key press (Function)

    model.generateShipLocations(); // calling from init means it loads right from the beginning of the game
}
    



